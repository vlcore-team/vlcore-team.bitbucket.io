# vectorlinux.com

Generated using Lektor

## Adding content

* Install Lektor: https://www.getlektor.com/docs/quickstart/
* Clone this repo and cd into it
* Run `lektor server`
* Edit your website at localhost:5000
* Run `git add` and `git commit` as usual
* Push to this repo
